### TO run Application
1- open terminal in the current folder
2- run command  'yarn install'
3- then run 'yarn start' to run the application
4- open browser and browse to 'http://localhost:3000' (if port 3000 is current in use then browse to next available port, to see this look in terminal where run "yarn start")


### TO use Application
1- open browser and browse to the url provided in terminal where 'yarn start' is run
2- home page contains a list of campaigns (to see full list scroll down)
3- add new tags by click in 'plus' icon in campaign after #tag section
4- search and apply filter( filters are fields or attributes stored in table)


###scripts 