import React from 'react';
import Campaign from "./app/campaign/Campaign";
import AddCampaign from "./app/campaign/AddCampaign";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Campaign} />
        <Route path="/add" component={AddCampaign} />
        <Route path="/edit/:id" component={AddCampaign} />
      </Switch>
    </Router>

  );
}

export default App;
