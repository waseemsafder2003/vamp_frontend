import {
    GET_CAMPAIGNS,
    SET_CAMPAIGNS_LOCAL,
    GET_ALL_CAMPAIGNS,
    SET_ALL_CAMPAIGNS_LOCAL,
    ADD_CAMPAIGNS,
    ADD_CAMPAIGNS_LOCAL,
    UPDATE_CAMPAIGNS,
    UPDATE_CAMPAIGNS_LOCAL,
    DELETE_CAMPAIGNS,
    DELETE_CAMPAIGNS_LOCAL,
    SET_CAMPAIGNS_LOADER,
    SET_MESSAGE
} from '../constants/ActionTypes';

export const getCampaign = (payload) => {
    return {
        type: GET_CAMPAIGNS,
        payload
    };
};
export const setCampaignLocal = (payload) => {
    return {
        type: SET_CAMPAIGNS_LOCAL,
        payload
    };
};

export const addCampaign = (payload) => {
    return {
        type: ADD_CAMPAIGNS,
        payload
    };
};
export const addCampaignLocal = (payload) => {
    return {
        type: ADD_CAMPAIGNS_LOCAL,
        payload
    };
};

export const updateCampaign = (payload) => {
    return {
        type: UPDATE_CAMPAIGNS,
        payload
    };
};
export const updateCampaignLocal = (payload) => {
    return {
        type: UPDATE_CAMPAIGNS_LOCAL,
        payload
    };
};
export const deleteCampaign = (payload) => {
    console.log("delete actopn")
    return {
        type: DELETE_CAMPAIGNS,
        payload
    };
};
export const deleteCampaignLocal = (payload) => {
    console.log("delete actopn")

    return {
        type: DELETE_CAMPAIGNS_LOCAL,
        payload
    };
};

export const getAllCampaigns = () => {
    return {
        type: GET_ALL_CAMPAIGNS,
    };
};
export const setAllCampaignsLocal = (payload) => {
    return {
        type: SET_ALL_CAMPAIGNS_LOCAL,
        payload
    };
};

export const setCampaignLoader = (payload) => {
    return {
        type: SET_CAMPAIGNS_LOADER,
        payload
    };
};
export const changeMessage = (payload) => {
    return {
        type: SET_MESSAGE,
        payload
    };
};


