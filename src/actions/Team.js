import {
   
    GET_ALL_TEAMS,
    SET_ALL_TEAMS_LOCAL,
    SET_TEAM_LOADER
} from '../constants/ActionTypes';



export const getAllTeams = () => {
    return {
        type: GET_ALL_TEAMS,
    };
};
export const setAllTeamsLocal = (payload) => {
    return {
        type: SET_ALL_TEAMS_LOCAL,
        payload
    };
};

export const setTeamLoader = (payload) => {
    return {
        type: SET_TEAM_LOADER,
        payload
    };
};



