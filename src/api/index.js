export const patch = async (url, obj) => {
    return await fetch(url, {
        method: 'PATCH',
        body: JSON.stringify(obj),
        headers: {
            "Content-type": "application/json"
        }
    })
        .then(reponse => reponse.json()).catch(e => {
            const error = e.message === "Failed to fetch" ? { error: new Error(e.message) } : e;
            return error;
        });
}
export const put = async (url, obj) => {
    return await fetch(url, {
        method: 'PUT',
        body: JSON.stringify(obj),
        headers: {
            "Content-type": "application/json"
        }
    })
        .then(reponse => reponse.json()).catch(e => {
            const error = e.message === "Failed to fetch" ? { error: new Error(e.message) } : e;
            return error;
        });
}
export const post = async (url, obj) => {
    return await fetch(url, {
        method: 'POST',
        body: JSON.stringify(obj),
        headers: {
            "Content-type": "application/json"
        }
    })
        .then(reponse => reponse.json()).catch(e => {
            const error = e.message === "Failed to fetch" ? { error: new Error(e.message) } : e;
            return error;
        });
}
export const get = async (url) => {
    return await fetch(url, {
        method: 'get',
        headers: {
            "Content-type": "application/json"
        }
    })
        .then(reponse => reponse.json()).catch(e => {
            const error = e.message === "Failed to fetch" ? { error: new Error(e.message) } : e;
            return error;
        });
}
