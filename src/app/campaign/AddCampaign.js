


import React from "react";
import DateTimePicker from 'react-datetime-picker';
import { getCampaign, addCampaign, updateCampaign, changeMessage } from "../../actions/Campaign";
import { getAllTeams } from "../../actions/Team";
import { connect } from 'react-redux';
import { Redirect } from "react-router-dom";
import Select from 'react-select';

class AddCampaign extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            start_date: new Date(),
            end_date: new Date(),
            id: '',
            edit: false,
            updated_at: new Date(),
            created_at: new Date(),
            team: {
                id: 1,
                name: "Prado"
            }
        };
    }

    componentDidMount() {
        if (this.props.match && this.props.match.params && this.props.match.params.id !== undefined) {
            this.props.getCampaign(this.props.match.params.id)
        }
        if (!this.props.tIsLoaded) {
            this.props.getAllTeams();
        }
    }
    handleChange = team => {
        this.setState({ team });
    }

    componentDidUpdate(prevP, prevS) {
        if (prevP.singleCampaign !== this.props.singleCampaign) {
            const { singleCampaign } = this.props;
            this.setState({
                id: singleCampaign.id,
                edit: true,
                campaign_name: singleCampaign.campaign_name,
                description: singleCampaign.description,
                campaign_id: singleCampaign.campaign_id,
                budget: singleCampaign.budget,
                tags: singleCampaign.tags,
                start_date:new Date(singleCampaign.start_date),
                end_date:new Date(singleCampaign.end_date),
                created_at: singleCampaign.created_at
            })
        }

        if (this.props.isAdded) {
            this.setState({
                goBack: true
            })
        }
        if (this.props.isFailed) {
            setTimeout(() => {
                this.props.changeMessage({ message: '', isFailed: false })
            }, 3000)
        }

    }

    getDate = (date) => {
        const d = new Date(date).toISOString().slice(0, 19).replace('T', ' ');
        return d;
    }
    onChange = date => this.setState({ start_date: date })
    addCampaigns = () => {
        const { campaign_id, campaign_name, description, budget, tags, start_date, end_date, created_at, updated_at, team } = this.state;
        const obj = {
            team_id: team.id,
            name: campaign_name,
            teamName: team.name,
         description, budget,
            tags, start_date: this.getDate(start_date),
            end_date: this.getDate(end_date), created_at: this.getDate(created_at),
            updated_at: this.getDate(updated_at)
        }


        if (this.state.edit) {
            obj.campaign_id = campaign_id;
            this.props.updateCampaign(obj);
        }
        else {
            this.props.addCampaign(obj);
        }
    }

    render() {
        const { edit, goBack, campaign_name, description, null_desc, null_name, null_tags, null_budget, budget, tags } = this.state;
        let { teamsArr } = this.props;
        if (goBack) {
            return <Redirect to="/" />
        }
        return (

            <div style={{ overflowX: 'hidden' }}>
                <div className="row topnav">
                    <div className="col-12 col-md-6 col-lg-6 col-sm-6 row" >
                        <h5 className="header">
                            {!edit ? " Add Campaign" : "Update Campaign"}
                        </h5>
                    </div>

                    <div className="col-6  col-md-3 col-lg-3 col-sm-3 my-1 select" >

                        <p className="text-danger" > {this.props.message}</p>
                    </div>

                </div>

                <div class="alert" role="alert" id="result"></div>
                <div className="form offset-lg-2  offset-md-2 col-md-8 col-lg-8 col-sm-12 col-12">
                    <div className="rounded " style={{ textAlign: 'left' }}>
                        <div className="card bg-light mb-3 shadow rounded" style={{ margin: '20px' }}>
                            <div className="card-body">
                                <div className="form-group">
                                    <label for="formGroupExampleInput">Campaign Name</label>
                                    <input type="text" className="form-control" placeholder="Enter Campaign Name"
                                        value={campaign_name}
                                        onChange={(e) => { this.setState({ campaign_name: e.target.value, null_name: false }) }}
                                    />
                                    {null_name ? <small style={{ color: 'red' }}>{"Enter valid Campaign name"}</small> : ''}
                                </div>


                                <div className="form-group">
                                    <label  >Budget </label>
                                    <input type="number" className="form-control" placeholder="Enter budget"
                                        value={budget}
                                        onChange={(e) => { this.setState({ budget: e.target.value, null_budget: false }) }}
                                    />
                                    {null_budget ? <small style={{ color: 'red' }}>{"Enter valid budget"}</small> : ''}

                                </div>

                                <div className="form-group">
                                    <label  >Tags </label>
                                    <input type="text" className="form-control" placeholder="Enter Tags"
                                        value={tags}
                                        onChange={(e) => { this.setState({ tags: e.target.value, null_tags: false }) }}
                                    />
                                    {null_tags ? <small style={{ color: 'red' }}>{"Enter valid tags"}</small> : ''}

                                </div>

                                <div className="form-group">
                                    <label  >Description </label>
                                    <textarea
                                        type="text" className="form-control" placeholder="Enter description"
                                        value={description}

                                        onChange={(e) => { this.setState({ description: e.target.value, null_desc: false }) }}
                                    />
                                    {null_desc ? <small style={{ color: 'red' }}>{"Enter valid tags"}</small> : ''}

                                </div>
                                <div className="form-group">
                                    <label  >Select Team </label>
                                    <Select
                                        options={teamsArr}
                                        value={this.state.team}
                                        onChange={this.handleChange}
                                        placeholder="Select Team"
                                    />
                                    {null_desc ? <small style={{ color: 'red' }}>{"Enter valid tags"}</small> : ''}

                                </div>
                                <div className="row">
                                    <div className="form-group col-12 col-md-6 col-lg-6">
                                        <label  >Start date </label>
                                        <br></br>
                                        <DateTimePicker
                                            onChange={(date) => this.setState({ start_date: date })}
                                            value={this.state.start_date}
                                        />
                                    </div>
                                    <div className="form-group col-12 col-md-6 col-lg-6">
                                        <label  >End date </label>
                                        <br></br>
                                        <DateTimePicker
                                            onChange={(date) => this.setState({ end_date: date })}
                                            value={this.state.end_date}
                                        />
                                    </div>
                                </div>
                                <div className="row mt-2">
                                    <div className="form-group col-12 col-md-6 col-lg-6">
                                        <button
                                            onClick={() => { this.setState({ goBack: true }) }}
                                            className="btn btn-outline-primary">{" Back "}</button>
                                    </div>
                                    <div className="form-group text-right col-12 col-md-6 col-lg-6">
                                        <button
                                            onClick={() => { this.addCampaigns() }}
                                            className="btn btn-outline-primary">{"Save "}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div >
        )
    }
}

const mapStateToProps = ({ campaign, teams }) => {
    const { teamsArr, isLoaded: tIsLoaded } = teams;
    const { isFailed, loader, message, isAdded, campaign: singleCampaign } = campaign;
    return { isFailed, loader, message, isAdded, singleCampaign, teamsArr, tIsLoaded };
};
export default connect(mapStateToProps, {
    getCampaign, addCampaign, updateCampaign, changeMessage, getAllTeams
})(AddCampaign);
