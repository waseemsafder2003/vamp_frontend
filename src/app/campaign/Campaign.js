import React from "react";
import SingleCampaign from "./SingleCampaign";
import Select from 'react-select';
import './CampaignStyle.css';
import { getAllCampaigns, setAllCampaignsLocal, deleteCampaign, updateCampaign } from "../../actions/Campaign";
import { connect } from 'react-redux';
import { Link } from "react-router-dom";

/* options for dropdown component
*
*/

const options = [
    { value: 'campaign_name', label: 'Campaign' },
    { value: 'budget', label: 'Budget' },
    { value: 'name', label: 'Team' },
    { value: 'tags', label: 'Tags' },
    { value: 'description', label: 'Description' }
];

class Campaign extends React.Component {
    constructor(p) {
        super(p);
        this.state = {
            selectedOption: options[0],
            searchValue: '',
            arr: [],
            campaignData: [],
            loaded: false,
            addtagsInputError: false,
            errorLoading: false
        };
    }




    componentDidMount() {
        document.title = "Campaigns";
        console.log(this.props.isLoaded)
        if (!this.props.isLoaded) {
            this.props.getAllCampaigns();
        }
        else {
            this.setState({ arr: this.props.campaignsArr, campaignData: this.props.campaignsArr })

        }
    }


    /* get All campaigns list 
    *
    */
    componentDidUpdate(prevP, prevS) {
        if (this.props.campaignsArr !== prevP.campaignsArr) {
            this.setState({ arr: this.props.campaignsArr, campaignData: this.props.campaignsArr })
        }
    }



    handleChange = selectedOption => {
        this.setState({ selectedOption });
    }


    /* Search and apply filter if there is no filter selected then filter on the base of name
    *
    */

    search = () => {
        const { searchValue } = this.state;
        const f = this.state.selectedOption && this.state.selectedOption.value ? this.state.selectedOption.value : 'name';
        if (searchValue === "") {
            this.setState({ campaignData: this.state.arr })
            return;
        }
        if (f === 'budget') {
            const data1 = this.state.arr.filter(d => {
                return d[f] === searchValue - 0;
            });
            this.setState({ campaignData: data1 });
        }
        else {
            const data1 = this.state.arr.filter(d => {
                return d[f].toUpperCase().includes(searchValue.toUpperCase());
            });
            this.setState({ campaignData: data1 });
        }
    }
    handleSearchChange = (e) => {
        this.setState({ searchValue: e.target.value }, this.search)

    }
    onKeyPress = (evt) => {
        if (evt.key === "Enter") {
            //  this.search();
        }
    }
    /* add new hashtag in campaign
    *  input: value:new entered tags and campaign id
    */

    addtags = (value, id) => {

        const { arr, campaignData } = this.state;
        const index = arr.findIndex(campaign => campaign.campaign_id === id);
        const index1 = campaignData.findIndex(campaign => campaign.campaign_id === id);
        let tags = arr[index].tags;
        const tagsArr = value.split('#');
        tagsArr.map(n => {
            tags = tags + ` #${n}`;
            return ''
        })
        arr[index].tags = tags;
        campaignData[index1].tags = tags;
        console.log(campaignData[index1]);
        this.setState({ arr, campaignData })
        this.props.updateCampaign(campaignData[index1]);
        // here call api
    }


    render() {
        const { campaignData, selectedOption, addtagsInputError } = this.state;
        const { loader, message } = this.props;
        return (<div style={{ overflowX: ' hidden' }}>
            <div className="row topnav">
                <div className="col-12 col-md-4 col-lg-4 col-sm-4 row" >
                    <h5 className="header">
                        {"Campaigns"}
                    </h5>
                </div>

                <div className="col-6  col-md-3 col-lg-3 col-sm-3 my-1 select" >

                    <Select
                        placeholder={"Select filter"}
                        isSearchable={true}
                        isClearable={true}
                        value={selectedOption}
                        onChange={this.handleChange}
                        options={options}
                    />
                </div>
                <div className="col-6 searchDiv col-md-3 col-lg-3 col-sm-3  border  row m-0 px-2 my-1 " >
                    <div className="col-10 p-0 m-0 pl-1 f" style={{ alignItems: 'center' }}>
                        <input onChange={this.handleSearchChange}
                            value={this.state.searchValue}
                            onKeyPress={(e) => this.onKeyPress(e)} type="text"
                            className="inputBox"
                            placeholder="Search.." name="search" />
                    </div>
                    <div className="col-2 d-flex  p-0 m-0 searchIcon" >
                        <i onClick={(e) => this.search()} style={{ fontSize: '20px' }} className="zmdi zmdi-search"></i>
                    </div>
                </div>
                <div className="add-btn-div col-2">
                    <div>
                        <Link to="/add">
                            <i className="zmdi zmdi-plus-circle zmdi-hc-2x" title="Add Campaign" />
                        </Link>
                    </div>
                </div>
            </div>




            {loader && message === "loading" ? <div className="loader">
                <div className="spinner-border text-primary">
                </div>
            </div> :

                campaignData.map((n, i) => {
                    return <SingleCampaign deleteCampaign={this.props.deleteCampaign} hashTagError={addtagsInputError} addtagFunc={this.addtags} key={i} data={n} />
                })
            }

        </div>
        )
    }
}




const mapStateToProps = ({ campaign }) => {
    const { isFailed, loader, message, campaignsArr, isLoaded } = campaign;
    return { isFailed, loader, message, campaignsArr, isLoaded };
};
export default connect(mapStateToProps, {
    getAllCampaigns, setAllCampaignsLocal, deleteCampaign, updateCampaign
})(Campaign);