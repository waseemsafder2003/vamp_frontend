import React, { useState } from "react";
import { Redirect } from "react-router-dom";

/* function for formated date
*  input: date
*/
function getFormattedDate(date1) {
    let date = new Date(Date.parse(date1));
    let year = date.getFullYear();
    let month = (1 + date.getMonth()).toString().padStart(2, '0');
    let day = date.getDate().toString().padStart(2, '0');
    return month + '-' + day + '-' + year;
}

/* functional component for single campaign
*  input props: data:{conatinig single campaign object , function to call Api for add tags , }
*/

function SingleCampaign({ data, addtagFunc, deleteCampaign }) {
    const [tags, sethashtag] = useState('');
    const [addtags, setAddtags] = useState(false);
    const [errorInput, setErrorInput] = useState(false);
    const [edit, setEdit] = useState(false);

    const addTags = () => {
        addtagFunc(tags, data.campaign_id)
        console.log("called")
        sethashtag('');
    }
    console.log(tags)
    if (edit) {
        return <Redirect to={`/edit/${data.campaign_id}`} />
    }
    return (<div className="rounded " style={{ textAlign: 'left' }}>
        <div className="card bg-light mb-3 shadow rounded" style={{ margin: '20px' }}>
            <div className="card-body">
                <div className="row">

                    <div className=" card-title col-12  col-md-9 col-lg-9 col-sm-9 ">
                        <div className="row">

                            <div className="col-12  col-md-9 col-lg-9 col-sm-9  ">
                                <h5>{data.campaign_name}</h5>
                                <p>
                                    {data.tags}
                                    {/* button to add new tags */}
                                    <i onClick={(e) => { setAddtags(!addtags) }} data-toggle="tooltip" data-placement="top" title="add hashtag" className="zmdi zmdi-plus-circle" style={{ fontSize: '20px', marginLeft: '5px' }}></i>

                                </p>
                                {addtags ?
                                    <div className="row">
                                        <div className="col-9">
                                            <input type="text" className="form-control" onChange={(e) => { sethashtag(e.target.value.trim()) }}
                                                value={tags}
                                                required
                                                placeholder="Enter text here" />
                                            {errorInput ? <small style={{ color: 'red' }}>{"Invalid hashtag entered"}</small> : ""}
                                        </div>
                                        <div className="col-3 px-1">
                                            <button 
                                            className="btn btn-outline-primary mr-1"
                                            onClick={(e) => { setAddtags(false) }} 
                                            >Cancel</button>
                                            <button onClick={() => {
                                                tags !== '' ?
                                                    addTags()
                                                    : setErrorInput(true)
                                            }} className="btn btn-outline-primary">{" Add "}</button>
                                        </div>
                                    </div>
                                    : ''}
                            </div>
                            <div className="col-12  col-md-3 col-lg-3 col-sm-3  d-flex" style={{ justifyContent: 'center', alignItems: 'center' }}>
                                budget: {data.budget}
                            </div>
                        </div>
                    </div>


                    <div className="row col-12 col-md-3 col-lg-3 col-sm-3"
                    >
                        <div className="col-2 p-0 m-0" >
                            <p> {'start'}</p>
                            <p> {'end'}</p>
                        </div>
                        <div className="col-7 m-0 p-0" style={{ paddingRight: '2%' }}>
                            <p >
                                :   {getFormattedDate(data.start_date)}

                            </p>
                            <p>
                                : {getFormattedDate(data.end_date)}
                            </p>
                        </div>
                        <div className="col-3 p-0 m-0" >
                            <p>
                                <i className="zmdi zmdi-edit text-primary" onClick={() => setEdit(true)} style={{ fontSize: '25px', cursor: 'pointer' }}></i>
                            </p>
                            <p>
                                <i className="zmdi zmdi-delete" onClick={() => deleteCampaign(data.campaign_id)} style={{ fontSize: '25px', color: 'red', cursor: 'pointer' }}></i>
                            </p>
                        </div>

                    </div>

                </div>

                <div className="row card-text">
                    <div className="col-12  col-md-9 col-lg-9 col-sm-9 ">
                        <p>{data.description}</p>
                    </div>
                    <div className="col-12  col-md-3 col-lg-3 col-sm-3  m-0 p-0">
                        <p>{"Team name:"}

                            {data.name}</p>
                    </div>
                </div>
            </div>
        </div>
    </div >)
}

export default SingleCampaign;