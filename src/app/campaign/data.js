
export const campaignData = [
    {
        id: 1,
        name: "Prado",
        team_id: 1,
        start_date: "2020-01-01 12:00:00",
        end_date: "2020-01-01 12:00:00",
        budget: 5000,
        tags: "#sneak #sneakers",
        campaign_name: "Sneak Sneakers",
        campaign_id: 1,
        created_at: "2020-04-01 08:13:18",
        updated_at: "2020-04-01 08:13:18"
    },
    {
        id: 1,
        name: "Prado",
        team_id: 1,
        start_date: "2020-01-01 12:00:00",
        end_date: "2020-01-01 12:00:00",
        budget: 4000,
        tags: "#glow #foundation",
        campaign_name: "Glow Foundation",
        campaign_id: 2,
        created_at: "2020-04-01 08:13:18",
        updated_at: "2020-04-01 08:13:18"
    },
    {
        id: 1,
        name: "Prado",
        team_id: 1,
        start_date: "2020 - 01 - 01 12: 00: 00",
        end_date: "2020 - 01 - 01 12: 00: 00",
        budget: 10000,
        tags: "#sleeptight #sheets",
        campaign_name: "Sleep Tight Sheets",
        campaign_id: 6,
        created_at: "2020 - 04 - 01 08: 13: 18",
        updated_at: "2020 - 04 - 01 08: 13: 18"
    },
    {
        id: 1,
        name: "Prado",
        team_id: 1,
        start_date: "2020-01-01 12:00:00",
        end_date: "2020-01-01 12:00:00",
        budget: 1000,
        tags: "#summer #sushi",
        campaign_name: "Summer Sushi",
        campaign_id: 8,
        created_at: "2020-04-01 08:13:18",
        updated_at: "2020-04-01 08:13:18"
    },
    {
        id: 1,
        name: "Prado",
        team_id: 1,
        start_date: "2020-01-01 12:00:00",
        end_date: "2020-01-01 12:00:00",
        budget: 7000,
        tags: "#illuminate #lights #fixtures",
        campaign_name: "Illuminate Lights",
        campaign_id: 9,
        created_at: "2020-04-01 08:13:18",
        updated_at: "2020-04-01 08:13:18"
    },
    {
        id: 1,
        name: "Prado",
        team_id: 1,
        start_date: "2020-01-01 12:00:00",
        end_date: "2020-01-01 12:00:00",
        budget: 4000,
        tags: "#sure #stationary #love",
        campaign_name: "Sure Stationary",
        campaign_id: 10,
        created_at: "2020-04-01 08:13:18",
        updated_at: "2020-04-01 08:13:18"
    },
    {
        id: 2,
        name: "Adidos",
        team_id: 2,
        start_date: "2020-01-01 12:00:00",
        end_date: "2020-01-01 12:00:00",
        budget: 8000,
        tags: "#applebabyfood #baby",
        campaign_name: "Apple Baby Food",
        campaign_id: 4,
        created_at: "2020-04-01 08:13:18",
        updated_at: "2020-04-01 08:13:18"
    }]









export const campaignData1 = [
    {
        name: 'Sneak Sneakers',
        id: 123,
        budget: 5000,
        hashtags: "#sneak #sneakers",
        team_id: 1,
        start_date: '2020-01-01 12:00:00',
        end_date: '2020-01-08 12:00:00',
        description: 'We want some awesome content of our new sneakers',
        team_name: 'Prado'
    },
    {
        id: 1234,
        name: 'Glow Foundation',
        budget: 4000,
        hashtags: "#glow #foundation",
        team_id: 1,
        start_date: '2020-01-01 12:00:00',
        end_date: '2020-01-08 12:00:00',
        description: 'Be creative and do some tutorials for our new foundation',
        team_name: 'Prado'
    },
    {
        id: 12345,
        name: 'Tiny Tshirts',
        budget: 2000,
        hashtags: "#tinytshirts",
        team_id: 1,
        start_date: '2020-01-01 12:00:00',
        end_date: '2020-01-08 12:00:00',
        description: 'Bring your whole self to the shoot for original content',
        team_name: 'Prado'
    },
    {
        id: 12443,
        name: 'Glow Foundation',
        budget: 4000,
        hashtags: "#glow #foundation",
        team_id: 1,
        start_date: '2020-01-01 12:00:00',
        end_date: '2020-01-08 12:00:00',
        description: 'Be creative and do some tutorials for our new foundation',
        team_name: 'Prado'
    },
    {
        id: 1244356,
        name: 'Tiny Tshirts',
        budget: 2000,
        hashtags: "#tinytshirts",
        team_id: 1,
        start_date: '2020-01-01 12:00:00',
        end_date: '2020-01-08 12:00:00',
        description: 'Bring your whole self to the shoot for original content',
        team_name: 'Prado'
    }
]
