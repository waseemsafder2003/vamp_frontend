import {
    SET_CAMPAIGNS_LOCAL,
    SET_ALL_CAMPAIGNS_LOCAL,
    ADD_CAMPAIGNS_LOCAL,
    UPDATE_CAMPAIGNS_LOCAL,
    DELETE_CAMPAIGNS_LOCAL,
    SET_CAMPAIGNS_LOADER,
    SET_MESSAGE
} from "../constants/ActionTypes";

const INIT_STATE = {
    loader: false,
    campaignsArr: [],
    campaign: {},
    message: "",
    isFailed: false,
    isAdded: false,
    isLoaded: false
};
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SET_ALL_CAMPAIGNS_LOCAL: {
            return {
                ...state,
                loader: false,
                message: "",
                isFailed: false,
                isLoaded: true,
                campaignsArr: action.payload
            }
        }
        case SET_MESSAGE: {
            return {
                ...state,
                loader: false,
                message: "",
                isFailed: false,
            }
        }
        case ADD_CAMPAIGNS_LOCAL: {
            const campaignsArr = [...state.campaignsArr];
            campaignsArr.push(action.payload);
            return {
                ...state,
                loader: false,
                message: "",
                isFailed: false,
                isAdded: true,
                campaignsArr
            }
        }
        case DELETE_CAMPAIGNS_LOCAL: {
            const campaignsArr = [...state.campaignsArr];
            const index = state.campaignsArr.findIndex(u => u.campaign_id === action.payload)
            if (index >= 0) {
                campaignsArr.splice(index, 1);
            }
            return {
                ...state,
                loader: false,
                message: "",
                isFailed: false,
                campaignsArr,
            }
        }
        case UPDATE_CAMPAIGNS_LOCAL: {
            const campaignsArr = [...state.campaignsArr];
            const index = state.campaignsArr.findIndex(u => u.campaign_id === action.payload.campaign_id)
            console.log(index);
          
            if (index >= 0) {
                campaignsArr[index] = action.payload;
            }
            return {
                ...state,
                loader: false,
                message: "",
                isFailed: false,
                isAdded: false,
                campaignsArr,
            }
        }
        case SET_CAMPAIGNS_LOADER: {
            const { loader, isFailed, message } = action.payload;
            return {
                ...state,
                loader,
                isFailed,
                message
            }
        }
        case SET_CAMPAIGNS_LOCAL: {
            return {
                ...state,
                loader: false,
                message: "",
                isFailed: false,
                isLoaded: true,
                campaign: action.payload
            }
        }
        default:
            return state;
    }
}