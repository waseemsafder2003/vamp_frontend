import { 
    SET_ALL_TEAMS_LOCAL,
    SET_TEAM_LOADER
} from "../constants/ActionTypes";

const INIT_STATE = {
    loader: false,
    teamsArr: [],
    message: "",
    isFailed: false,
    isAdded: false,
    isLoaded: false
};
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SET_ALL_TEAMS_LOCAL: {
            return {
                ...state,
                loader: false,
                message: "",
                isFailed: false,
                isLoaded: true,
                teamsArr: action.payload
            }
        }
        case SET_TEAM_LOADER: {
            const { loader, isFailed, message } = action.payload;
            return {
                ...state,
                loader,
                isFailed,
                message
            }
        }
       
        default:
            return state;
    }
}