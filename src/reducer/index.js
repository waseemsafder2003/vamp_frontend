import { combineReducers } from 'redux';
import Campaign from './Campaign';
import Team from './Team';

export default () => combineReducers({
  campaign: Campaign,
  teams : Team
});
