
import {
    GET_CAMPAIGNS,
    GET_ALL_CAMPAIGNS,
    ADD_CAMPAIGNS,
    UPDATE_CAMPAIGNS,
    DELETE_CAMPAIGNS,
} from '../constants/ActionTypes';

import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import { API_BASE_URL } from "../constants/config"

import {
    addCampaignLocal, setAllCampaignsLocal, updateCampaignLocal, deleteCampaignLocal, setCampaignLoader, setCampaignLocal
} from "../actions/Campaign";
import { get, putCall, deleteCall, post } from "../constants/apicall";//pass url to get,and (url,object) to post ,patch



function* createCampaignSaga({ payload }) {
    try {
        console.log(payload)

        yield put(setCampaignLoader({ isFailed: false, message: "", loader: true }));
        const url = `${API_BASE_URL}campaigns/add`;
        const obj = { ...payload };
        delete obj.id;
        const teamName= obj.teamName;
        delete obj.teamName;
        const response = yield call(post, url, obj);
        console.log(response)
        if (!response.error) {
            const data = { ...response.data };
            data.campaign_id = response.data.id;
            data.campaign_name = response.data.name;
            data.id = response.data.id;
            data.name = teamName;
            yield put(addCampaignLocal(data));
        }
        else {
            yield put(setCampaignLoader({ isFailed: true, message: response.error.message, loader: false }))
        }
    } catch (error) {
        yield put(setCampaignLoader({ isFailed: true, message: error.message, loader: false }))
    }
}
function* updateCampaignSaga({ payload }) {
    try {
        yield put(setCampaignLoader({ isFailed: false, message: "", loader: true }));
        const { campaign_id } = payload;
        const url = `${API_BASE_URL}campaigns/update/${campaign_id}`;
        const obj = { ...payload };
        delete obj.campaign_id;
        const teamName= obj.teamName;
        delete obj.teamName;
        const response = yield call(putCall, url, obj);
        if (!response.error) {
            const data = { ...response.data };
            data.campaign_id = response.data.id;
            data.campaign_name = response.data.name;
            data.id = response.data.id;
            data.name = teamName;
            yield put(updateCampaignLocal(data));
        }
        else {
            yield put(setCampaignLoader({ isFailed: true, message: "Failed to Update", loader: false }))
        }
    } catch (error) {
        yield put(setCampaignLoader({ isFailed: true, message: error.message, loader: false }))
    }
}

function* deleteCampaignSaga({ payload }) {
    try {
        yield put(setCampaignLoader({ isFailed: false, message: "", loader: true }));
        const url = `${API_BASE_URL}campaigns/delete/${payload}`;
        const response = yield call(deleteCall, url);
        if (response.error) {
            yield put(setCampaignLoader({ isFailed: true, message: response.error.message, loader: false }))
        }
        else {
            yield put(deleteCampaignLocal(payload));

        }
    } catch (error) {
        yield put(setCampaignLoader({ isFailed: true, message: error.message, loader: false }))
    }
}


function* getCampaignsSaga() {
    try {
        yield put(setCampaignLoader({ isFailed: false, message: "loading", loader: true }));

        const url = `${API_BASE_URL}campaigns`;
        const response = yield call(get, url);
        if (!response.error) {
            yield put(setAllCampaignsLocal(response.data));
        }
        else {
            yield put(setCampaignLoader({ isFailed: true, message: response.error.message, loader: false }))
        }
    } catch (error) {
        yield put(setCampaignLoader({ isFailed: true, message: error.message, loader: false }))
    }
}


function* getCampaignSaga({ payload }) {
    try {
        yield put(setCampaignLoader({ isFailed: false, message: "", loader: true }));

        const url = `${API_BASE_URL}campaigns/${payload}`;
        const response = yield call(get, url);
        if (!response.error) {
            yield put(setCampaignLocal(response.data));
        }
        else {
            yield put(setCampaignLoader({ isFailed: true, message: response.error.message, loader: false }))
        }
    } catch (error) {
        yield put(setCampaignLoader({ isFailed: true, message: error.message, loader: false }))
    }
}


export function* requestCreateCampaign() {
    yield takeLatest(ADD_CAMPAIGNS, createCampaignSaga);
}


export function* requestUpdateCampaign() {
    yield takeLatest(UPDATE_CAMPAIGNS, updateCampaignSaga);
}

export function* requestGetCampaigns() {
    yield takeLatest(GET_ALL_CAMPAIGNS, getCampaignsSaga);
}

export function* requestDeleteCampaign() {
    yield takeLatest(DELETE_CAMPAIGNS, deleteCampaignSaga);
}

export function* requestGetCampaign() {
    yield takeLatest(GET_CAMPAIGNS, getCampaignSaga);
}
export default function* rootSaga() {
    yield all([
        fork(requestCreateCampaign),
        fork(requestDeleteCampaign),
        fork(requestGetCampaigns),
        fork(requestUpdateCampaign),
        fork(requestGetCampaign),
    ]);

}