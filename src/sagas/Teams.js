
import {
   
    GET_ALL_TEAMS,
 
} from '../constants/ActionTypes';

import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import { API_BASE_URL } from "../constants/config"

import {
    setAllTeamsLocal, setTeamLoader
} from "../actions/Team";
import { get} from "../constants/apicall";//pass url to get,and (url,object) to post ,patch





function* getTeamsSaga() {
    try {
        yield put(setTeamLoader({ isFailed: false, message: "loading", loader: true }));
        const url = `${API_BASE_URL}team`;
        const response = yield call(get, url);
        if (!response.error) {
            const data=response.data.map(t=>{
                return { ...t,label : t.name,value : t.name}
            })
            console.log(data)
            yield put(setAllTeamsLocal(data));
        }
        else {
            yield put(setTeamLoader({ isFailed: true, message: response.error.message, loader: false }))
        }
    } catch (error) {
        yield put(setTeamLoader({ isFailed: true, message: error.message, loader: false }))
    }
}


export function* requestGetTeams() {
    yield takeLatest(GET_ALL_TEAMS, getTeamsSaga);
}

export default function* rootSaga() {
    yield all([
      
        fork(requestGetTeams),
       
    ]);

}