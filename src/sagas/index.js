import { all } from 'redux-saga/effects';
import campaignSagas from './Camaign';
import teamSaga from "./Teams";
export default function* rootSaga(getState) {
    yield all([
        campaignSagas(),
        teamSaga()
    ]);
}
